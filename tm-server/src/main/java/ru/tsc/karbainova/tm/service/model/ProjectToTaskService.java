package ru.tsc.karbainova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.karbainova.tm.api.repository.model.ProjectRepository;
import ru.tsc.karbainova.tm.api.repository.model.TaskRepository;
import ru.tsc.karbainova.tm.api.service.model.IProjectToTaskServiceModel;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.model.User;

import java.util.List;

import static org.reflections.util.Utils.isEmpty;

@Service
public class ProjectToTaskService implements IProjectToTaskServiceModel {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Override
    @Transactional
    @SneakyThrows
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        User user = projectRepository.findById(projectId).get().getUser();
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        taskRepository.bindTaskById(user, projectId, taskId);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void unbindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        User user = projectRepository.findById(projectId).get().getUser();
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(taskId)) throw new EmptyIdException();
        taskRepository.unbindTaskById(user, projectId, taskId);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Task> findTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (isEmpty(userId)) throw new EmptyIdException();
        return taskRepository.findTasksByUserIdProjectId(userId, projectId);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeProjectById(@Nullable final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        taskRepository.removeTasksByProjectId(id);
        projectRepository.removeById(id);
    }
}
