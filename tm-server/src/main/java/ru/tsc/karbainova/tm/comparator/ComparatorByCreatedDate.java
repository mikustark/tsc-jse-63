package ru.tsc.karbainova.tm.comparator;

import ru.tsc.karbainova.tm.api.entity.IHasDateCreated;

import java.util.Comparator;

public class ComparatorByCreatedDate implements Comparator<IHasDateCreated> {

    private static final ComparatorByCreatedDate instance = new ComparatorByCreatedDate();

    private ComparatorByCreatedDate() {

    }

    public static ComparatorByCreatedDate getInstance() {
        return instance;
    }

    @Override
    public int compare(final IHasDateCreated o1, final IHasDateCreated o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
