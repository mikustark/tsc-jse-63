<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Task Create</h1>

<form action="/task/create/?id=${task.id}" method="post">
    <input type="hidden" name="id" value="${task.id}"/>

    <p>
    <div>Project ID:</div>
    <div><select id="projectId" name="projectId">
        <c:forEach var="project" items="${projects}">
            <option value=<c:out value="${project.id}"/>><c:out value="${project.id} | ${project.name}"/></option>
        </c:forEach>
    </select>
    </div>

    <p>
    <div>Name:</div>
    <div><input type="text" name="name" value="${task.name}"/></div>

    <p>
    <div>Description:</div>
    <div><input type="text" name="description" value="${task.description}"/></div>

    <p>
    <div>Status:</div>
    <div><input type="text" name="status" value="${task.status}" readonly="readonly"/></div>

    <p>
    <div>Start Date:</div>
    <div><input type="datetime-local" name="startDate" value="${task.startDate}" required="required"/></div>

    <p>
    <div>Finish Date:</div>
    <div><input type="datetime-local" name="finishDate" value="${task.finishDate}" required="required"/></div>

    <button type="submit">Create Task</button>
</form>

<jsp:include page="../include/_footer.jsp"/>
