<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Project Create</h1>

<form action="/project/create/?id=${project.id}" method="post">
    <input type="hidden" name="id" value="${project.id}"/>

    <p>
    <div>Name:</div>
    <div><input type="text" name="name" value="${project.name}"/></div>

    <p>
    <div>Description:</div>
    <div><input type="text" name="description" value="${project.description}"/></div>

    <p>
    <div>Status:</div>
    <div><input type="text" name="status" value="${project.status}" readonly="readonly"/></div>

    <p>
    <div>Start Date:</div>
    <div><input type="datetime-local" name="startDate" value="${project.startDate}" required="required"/></div>

    <p>
    <div>Finish Date:</div>
    <div><input type="datetime-local" name="finishDate" value="${project.finishDate}" required="required"/></div>

    <button type="submit">Create Project</button>
</form>

<jsp:include page="../include/_footer.jsp"/>
