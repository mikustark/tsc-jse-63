package ru.tsc.karbainova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Project{

    @NotNull
    protected String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private String status = Status.NOT_STARTED.getDisplayName();

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @NotNull
    private Date created = new Date();

    @NotNull
    public Project(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public Project(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }

}
