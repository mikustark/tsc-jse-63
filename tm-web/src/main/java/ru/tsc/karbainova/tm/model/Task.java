package ru.tsc.karbainova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Task {

    @NotNull
    protected String id = UUID.randomUUID().toString();

    @NonNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private String status = Status.NOT_STARTED.getDisplayName();

    @Nullable
    private Date startDate;

    @Nullable
    private Date finishDate;

    @Nullable
    private String projectId;

    @NotNull
    private Date created = new Date();

    @NotNull
    public Task(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public Task(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
    }

}
