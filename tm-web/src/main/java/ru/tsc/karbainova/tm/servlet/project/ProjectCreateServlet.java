package ru.tsc.karbainova.tm.servlet.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/project/create/*")
public class ProjectCreateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @Nullable final Project project = ProjectRepository.getInstance().create();
        req.setAttribute("project", project);
        req.getRequestDispatcher("/WEB-INF/views/project-create.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String description = req.getParameter("description");
        @NotNull final String status = req.getParameter("status");
        @NotNull final String startDate = req.getParameter("startDate");
        @NotNull final String finishDate = req.getParameter("finishDate");
        @NotNull final Project project = new Project();
        Date start = date.parse(startDate);
        Date finish = date.parse(finishDate);
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setStatus(status);
        project.setStartDate(start);
        project.setFinishDate(finish);
        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }

}

